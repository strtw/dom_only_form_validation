// your code goes here ...
'use strict';
var householdBuilder = (function householdBuilder() {  //main household builder module
    
    /** 
    * Function that initialize the Household Builder App
    */
    function init(){
        var householdData = { //structure to maintain the state of the household data
            household: []
        };
        var household = householdData.household;
        
        //DOM selections
        var ageInput = document.querySelector('[name=age]');
        var form = document.querySelector('form');
        var relationshipInput = document.querySelector('[name=rel');
        var smoker = document.querySelector('[name=smoker]');
        var submitButton = document.querySelector('[type=submit]');
        var output = document.querySelector('.debug');
        
        //New elements to add to DOM
        var ageErrorEl = document.createElement('pre');
        var relationshipErrorEl = document.createElement('pre');
        var houseHoldDiv = document.createElement('div');
        
        //Set error styling
        ageErrorEl.style.color = 'red';
        ageErrorEl.style.margin = '15px';
        ageErrorEl.innerHTML = ' ';
        relationshipErrorEl.style.color = 'red';
        relationshipErrorEl.style.margin = '15px';
        relationshipErrorEl.innerHTML = ' ';
        
        //insert error placeholder into DOM
        ageInput.parentNode.insertAdjacentElement('afterend', ageErrorEl);
        relationshipInput.parentNode.insertAdjacentElement('afterend', relationshipErrorEl);
        
        //Disable submit button on load
        submitButton.disabled = true;
    
        //Disable JSON data output view on load
        output.style.display = 'none';
        
        //set input to only accept numbers
        ageInput.setAttribute('type','number');
    
        //create div to append household data to
        houseHoldDiv.setAttribute('id', 'households');
        
        //append to form
        form.appendChild(houseHoldDiv); 
        
        form.addEventListener('click', function(event) {
            addPersonIf(event);
            removePerson(event);
            if(event.target.type !== 'checkbox'){
                event.preventDefault(); //prevent default form submission button behavior to prevent page navigation
            }
        
        });
    
        submitButton.addEventListener('click', function(event) {
            displayHouseholdData(event);
            event.preventDefault();
        })
        
        ///HELPER FUNCTIONS BELOW///
    
        /** 
         * Function to add person to the application if inputs are valid
        */
        function addPersonIf(){
            var person;
            var householdFragment;
            var inputsValid; 
            if(event.target.className === 'add'){
                inputsValid = testInputValidity(); 
                if (inputsValid.result) {// if inputs are value
                    person = createPerson(); //create a person
                    addToHousehold(person, household); // add the person to the household
                    householdFragment = createHouseHoldFragment(household); //add the household data to a document fragment
                    addHouseholdToDOM(householdFragment); //update the dom with the document fragment contents
                    submitButton.disabled = false; //allow user to submit data
                } 
                ageErrorEl.innerHTML = inputsValid.ageError //display the error	
                relationshipErrorEl.innerHTML = inputsValid.relationshipError; //display the error	
            }
            
        }
    
        /** 
         * Function to test the validity of the Household Builder form
        */
        function testInputValidity() { 
            var result = {
                result: true //set state as form is valid
            };
            //Handle age validity
            if (ageInput.value <= 0 || ageInput.value === ' ') { //if age is less than zero or input is blank
                result.result = false; //form is invalid
                result.ageError = 'Please enter an age greater than 0'; //set the error
            } else if (ageInput.value > 130) { //if age is too old
                result.result = false;// form is invalid
                result.ageError = 'Please enter a realistic age' // set the error
            } else {
               // ageErrorEl.innerHTML = ' '; //if form is valid, set 
                result.ageError = ' ';
            }
            
            //Handle relationship validity
            if (relationshipInput.options[relationshipInput.selectedIndex].value === '') { //if no value is selected
                result.result = false; // form is invalid
                result.relationshipError = 'Please select a value'; //set the error
            } else {
                result.relationshipError = ' '; //set error value to blank
            }
            return result;
        }
    
        /** 
         * Function to create a person for the Household Builder
        */
        function createPerson() {
            var person = {};
            var timestamp;
            
            timestamp = new Date();
            person.age = ageInput.value;
            person.relationship = relationshipInput.value;
            person.smoker = smoker.checked;
            person.id = timestamp.getTime();
            return person;
        }
    
         /** 
         * Function to a a person the Household builder
        */
        function addToHousehold(person, household) {
            household.push(person);
            return household;
        }
    
        /** 
         * Function that creates a document fragment table from the household data
        */
        function createHouseHoldFragment(household) {
            var person;
            var tableRow;
            var tableData;
            var rowClearControl;
            var fragment;
            var table;
            var tableHeader;
            var tableHeadAge;
            var tableHeadRelationship;
            var tableHeadSmoker;
            if (household) {
                fragment = document.createDocumentFragment();
                table = document.createElement('table');
                tableHeader = document.createElement('tr');
                tableHeadAge = document.createElement('th');
                tableHeadRelationship = document.createElement('th');
                tableHeadSmoker = document.createElement('th');

                table.setAttribute('id', 'household-data');
                
                tableHeadAge.innerHTML = 'Age';
                tableHeader.appendChild(tableHeadAge);
    
                tableHeadRelationship.innerHTML = 'Relationship';
                tableHeader.appendChild(tableHeadRelationship);
                
                tableHeadSmoker.innerHTML = 'Smoker?';
                tableHeader.appendChild(tableHeadSmoker);
               
                table.appendChild(tableHeader);
                
                for (var i = 0; i < household.length; i++) {//loop through household data and create row for each person
                    person = household[i];
                    tableRow = document.createElement('tr'); 
                    tableRow.setAttribute('id', person.id);
                    for (var attribute in person) {//loop through each person and add attributes as table cells
                        tableData = document.createElement('td');
                        if (attribute !== 'id') {//exclude ID from cells
                            tableData.innerHTML = person[attribute];
                        }
                        tableRow.appendChild(tableData);//append row to table
                    }
                    rowClearControl = document.createElement('button');//add a remove button for each person
                    rowClearControl.innerHTML = 'remove';
                    rowClearControl.setAttribute('class', 'row-clear');
                    tableRow.appendChild(rowClearControl);
                    table.appendChild(tableRow);
                }
                fragment.appendChild(table);
            }
            return fragment;
        }
    
         /** 
         * Function that removes person from app
        */
        function removePerson(){
            if (event.target.className === 'row-clear') {
                removePersonFromHouseholdData(event);
                removePersonFromDOM(event)
            }
        }
    
    
        /** 
         * Function that removes person from household data when remove button is clicked
        */
        function removePersonFromHouseholdData(event){
                for (var j = household.length - 1; j >= 0; --j) {//loop through people
                    if (event.target.parentNode.id == household[j].id) {//find the person in the household that corresponds to the clicked row
                        household.splice(j, 1);//remove person from the household
                        break;
                    }
                }
            
            
        }
        
        /** 
         * Function that removes person from DOM data when remove button is clicked
        */
        function removePersonFromDOM(event){
            var householdFragment;
            if (event.target.className === 'row-clear') {//if remove button is clicked
                householdFragment = createHouseHoldFragment(household);//create a new table with updated household data
                addHouseholdToDOM(householdFragment);//append the table to the DOM
                output.style.display = 'none'; //remove the JSON submit data from view so user knows they are on new submission
                if (household.length === 0) {//if there are no people in the household 
                    submitButton.disabled = true; //disable the submit button
                }
            }
        }
    
        /** 
         * Function that updates adds the household to the DOM
        */
        function addHouseholdToDOM(household) {
            if (houseHoldDiv.children.length > 0) {
                houseHoldDiv.removeChild(houseHoldDiv.lastElementChild);
            }
            houseHoldDiv.appendChild(household);
        }
        
         /** 
         * Function that dislays the household data on the page
        */
        function displayHouseholdData(event){
            if(event.target.type === 'submit'){
                var data = JSON.stringify(householdData);
                output.style.display = 'block';
                output.innerHTML = data;
            }
          
        }
    }
    
    return{
        init:init //publicly available method
    }
})();

householdBuilder.init();